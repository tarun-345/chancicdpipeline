package main

import "testing"

func TestHelloWorldString(t *testing.T){
  if getHelloWorldString() != "We are at Great Place to Work"{
    t.FailNow()
  }
}
